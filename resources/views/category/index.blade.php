@extends('master')

{{-- <style>
    .name{
        display:flex;
        align-items: center;
        flex-direction: row;
    }
    .name form{
        margin-bottom: 0;
        margin-left: 10px;
    }
</style> --}}

@section('content')
<h3> List Category </h3>
    <table border=1px;>
        <tr>
            <td>Id</td>
            <td>Category Name</td>
            <td>Action</td>
        </tr>
        
        @foreach ($categories as $category)
            <tr>
                <td> {{ $category->id}} </td>
                <td> {{ $category->category_name}} </td>
                <td>
                    <form action="{{ route('category.destroy',$category->id ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button class="btn btn-danger"> Hapus </button>
                    </form>
                </td>
            </tr>
        @endforeach
        
    </table>

@endsection