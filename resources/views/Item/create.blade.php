@extends('master')

@section('content')

<h3> Create Item</h3>
    <form action="{{ route('item.store') }}" method="POST">
        @csrf
        <div>
            <label for="category"> Select Category : </label>
            <select name="category_id" id="category">
                @foreach ($categories as $id => $name)
                    <option value="{{ $id }}"> {{ $name }} </option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="name"> Item Name : </label>
            <input type="text" name="item_name" id="name">
        </div>
        <div>
            <button type="submit"> Create </button>
        </div>
    </form>

@endsection