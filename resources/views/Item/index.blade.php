@extends('master')

<style>


</style>

@section('content')

<table border=1px; text-align = center; margin-right="10px">
    <tr>
      <th>ID</th>
      <th>CategoryID</th>
      <th>Category</th>
      <th>Item</th>
      <th>Action</th>
    </tr>
    @foreach ($items as $item )
      <tr>
        <td>{{ $item->id}}</td>
        <td>{{ $item->category_id}}</td>
        <td>{{ $item->category->category_name}}</td>
        <td>{{ $item->item_name}}</td>
        <td>
          <a href="#" class="btn btn-success editbtn"> Edit </a>
        
            <button type="submit" class="btn btn-danger deletebtn"> Delete </button>
        </td>
      </tr>
    @endforeach
  </table>
  
  {{-- <!-- Add Item Modal --> --}}
  <div class="modal fade" id="createItemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form action="{{ route('item.store') }}" method="POST" id="addform">
            <div class="modal-body">
                @method('POST')
                @csrf
                <div>
                    <label for="name"> Item Name : </label>
                    <input type="text" name="item_name" id="name">
                </div>
                <div>
                    <label for="category"> Select Category : </label>
                    <select name="category_id" id="category">
                        @foreach ($categories as $id => $name)
                            <option value="{{ $id }}"> {{ $name }} </option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
          </form>
      </div>
    </div>
  </div>

  {{-- Edit Item Modal  --}}

  <div class="modal fade" id="editItemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="#" method="POST" id="editform">
          <div class="modal-body">
          @METHOD('PUT')
            @csrf
            <div>
              <input type="hidden" value="{{ $item->id }}" name="id" id="id">
            </div>
            <div>
              <div>
                <label for="category"> Select Category : </label>
                <select name="category_id" id="category_id" class="category_id">
                    @foreach ($categories as $id => $name)
                    ///isi tag option id karena kalau pake name get value ajax nya error
                      <option selected={{ $item->category_id == $id}} value="{{ $id }}"> {{ $id }} </option>
                    @endforeach
                </select>
              </div>
              <div>
                <label for="item"> Item Name : </label>
                <input type="text" name="item_name" id="item" value="{{ $item->item_name }}">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  {{-- Delete Item Modal  --}}

  <div class="modal fade" id="deleteItemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete Item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
          <form method="POST" name="deleteform" id="deleteItem">
            @method('DELETE')
            @csrf
            <div>
              <p>    Are you sure to delete item? </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Delete</button>
            </div>
          </form>
      </div>
    </div>
  </div>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createItemModal">
    Create Item
  </button>

  
@endsection