@extends('master')


@section('content')
    
<form action="{{ route('item.update', $item->id) }}" method="POST">
    @method('PUT')
    @csrf
    <div>
        <label for="category"> Select Category : </label>
        <select name="category_id" id="category">
            @foreach ($categories as $id => $name)
                <option selected={{ $item->category_id == $id}} value="{{ $id }}"> {{ $name }} </option>
            @endforeach
        </select>
    </div>
    <div>
        <label for="name"> Item Name : </label>
        <input type="text" name="item" id="name" value="{{ $item->item_name }}">
    </div>
    <div>
        <button type="submit"> Edit </button>
    </div>
</form>
    
@endsection