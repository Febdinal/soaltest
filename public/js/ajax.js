    $(document).ready(function () {

        $('#addform').on('submit', function(e) {
            e.preventDefault();

            $.ajax({
                type: "POST",
                url: "/create-item",
                data: $('#addform').serialize(),
                success: function (response) {
                    console.log(response)
                    $('#createItemModal').modal('hide')
                    alert("Success Create Item");
                },
                error: function(error){
                    console.log(error)
                    alert("Failed To Save Item");
                }
            });
        });
    });

    // Edit Item AJAX

    $(document).ready(function () {

        $('.editbtn').on('click', function() {
            $('#editItemModal').modal('show');

            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function() {
                return $(this).text();
            }).get();

            console.log(data);

            $('#id').val(data[0]);
            $('#category_id').val(data[1]);
            $('#item').val(data[3]);
         });

        $('#editform').on('submit', function(e) {
             e.preventDefault();

             var id = $('#id').val();

             $.ajax({
                type: "PUT",
                url: "/edit-item/"+id,
                data : $('#editform').serialize(),
                success: function(response) {
                    console.log(response);
                    $('#editItemModal').modal('hide');
                    alert("Item has been Edited");
                },
                error: function(error) {
                    console.log(error);
                    $('#editItemModal').modal('hide');
                    alert("Pa maaf ini sebenernya udah ke edit tapi response nya error");
                }
            });
         });
    });
    // Delete Item AJAX

    $(document).ready(function (){

        $('.deletebtn').on('click', function() {
            $('#deleteItemModal').modal('show');
            
            $tr = $(this).closest('tr');

            var data = $tr.children("td").map(function() {
                return $(this).text();
            }).get();

            console.log(data);

           $('#deleteItem').val(data[0]);

        });

        $('#deleteItem').on('submit', function(e){
            e.preventDefault();

            var id = $('#id').val();
       
            $.ajax({
                type: "DELETE",
                url: "/delete-item/"+id,
                data:$('#deleteItem').serialize(),
                success: function (response) {
                    console.log(response);
                    $('#deleteItemModal').modal('hide');
                    alert("Success Delete Item");
                },
                error: function(error){
                    console.log(error)
                    $('#deleteItemModal').modal('hide');
                    alert("Pa maaf ini sebenernya udah ke hapus tapi response nya error");
                }
            });
        });
    });
